#!/bin/bash
python -V  # Print out python version for debugging
pip install pipenv
pipenv install --dev -v
pipenv run python -V
