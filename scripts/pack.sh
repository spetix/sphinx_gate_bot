#!/bin/bash

pipenv lock -r > requirements.txt
pipenv run python setup.py bdist_wheel
