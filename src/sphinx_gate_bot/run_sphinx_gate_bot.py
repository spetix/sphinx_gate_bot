import argparse
import json

import daemon
import logging
import sys


from sphinx_gata_bot.SphinxGateBot import SphinxGateBot
from sphinx_gata_bot.CondfigReader import CondfigReader

LEVELS = {'DEBUG': logging.DEBUG,
          'INFO': logging.INFO,
          'WARN': logging.WARNING,
          'ERROR': logging.ERROR}


def create_parser():
    parser = argparse.ArgumentParser(description="T-Bot for local net")
    parser.add_argument("--secrets", "-s",
                        help='file containing vaulted token',
                        type=open,
                        required=True)
    parser.add_argument("--model", "-m",
                        help='file containing model definition',
                        type=open,
                        required=True)
    parser.add_argument('--passphrase', '-p',
                        help='passphrase to use key',
                        default=None,
                        required=False)
    parser.add_argument('--key', '-k',
                        help='key to decrypt token',
                        default=None,
                        required=False)
    parser.add_argument('--daemonize', '-d',
                        help='run bot as a daemon service',
                        default=False,
                        required=False)
    parser.add_argument('--loglevel', '-l',
                        help='run bot as a daemon service',
                        default='INFO',
                        choices=LEVELS.keys(),
                        required=False)
    parser.add_argument('--logfile', '-f',
                        help='logfile name',
                        default='./sphinx_gate_bot.log',
                        required=False)
    return parser


def do_main_program(params, model):
    myBot = SphinxGateBot(params, model)
    myBot.listen()


def main():
    args = create_parser().parse_args()
    config = ConfigReader(args.secrets)
    params = config.decrypt(args.key, args.passphrase)
    model = config.getModel()
    logging.basicConfig(filename=args.
                        logfile,
                        level=LEVELS[args.loglevel])

    if args.daemonize:
        with daemon.DaemonContext():
            do_main_program(params, model)
    else:
        do_main_program(params, model)


if __name__ == '__main__':
    main()
