'''
Created on 31 mag 2019

@author: ccassano
'''
import requests
import telebot
import logging

STATUS_DICT = {False: 'Off', True: 'On'}


class SphinxGateBot(object):

    def __init__(self, params, model):
        self.bot = telebot.TeleBot(params['token'])
        self.__ssh_status = True
        self.__params = params
        self.__model = model

        @self.bot.message_handler(commands=['ip', 'IP', 'Ip', 'iP'])
        def send_get_ip(message):
            self.bot.reply_to(message,
                              "router ip is %s" % self.what_is_router_ip())

        @self.bot.message_handler(commands=['ssh',
                                            'ssH',
                                            'sSH',
                                            'SSH',
                                            'SSh',
                                            'Ssh'])
        def send_ssh(message):
            self.bot.reply_to(message, "%s" % self.do_ssh(message))

    def what_is_router_ip(self):
        ext_ip = requests.get("https://api.ipify.org").text
        return ext_ip

    def ssh(self, turnOn):
        cookies = self.translate(self.__model['cookies'])
        logging.debug('traslated cookies %s', str(cookies))

        headers = self.translate(self.__model['headers'])
        logging.debug('traslated cookies %s', str(headers))

        data = self.__model['data'] % \
            self.__model['ssh_on'] if turnOn else self.__moedle['ssh_off']
        logging.debug('traslated cookies %s', str(data))

        route = self.__params['route'] % self.__params
        logging.debug('traslated cookies %s', str(route))

        response = requests.post(route,
                                 headers=headers,
                                 cookies=cookies,
                                 data=data)
        if turnOn:
            self.__ssh_status = True
        else:
            self.__ssh_status = False
        return iself.get_ssh_status()

    def get_ssh_status(self):
        return STATUS_DICT.get(self.__ssh_status)

    def translate(self, list_to_fill):
        logging.debug('data %s', list_to_fill)
        processed_list = dict()
        for itKey, itValue in list_to_fill.iteritems():
            processed_list[itKey] = itValue % self.__params
        return processed_list

    def do_ssh(self, message):
        args = message.text.strip().lower().split()
        if len(args) == 0:
            return self.ssh(self.__ssh_status)
        if len(args) != 2:
            return 'only one argument is supported [on|off|status]'
        elif args[1] == 'on':
            return self.ssh(True)
        elif args[1] == 'off':
            return self.ssh(False)
        elif args[1] == 'status':
            return self.get_ssh_status()
        else:
            answer = 'unknown argument %s' % args[1]
        return answer

    def listen(self):
        self.bot.polling(none_stop=True,
                         interval=0,
                         timeout=20)
