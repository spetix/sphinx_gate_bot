# setup.py
from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()[1:]


setup(setup_requires=["pbr"],
      pbr=True,
      install_requires=requirements,
      )
